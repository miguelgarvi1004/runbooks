module gitlab.com/gitlab-com/runbooks/pingdom

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russellcardullo/go-pingdom v0.0.0-20181021024747-0897d314d9a6
	github.com/stretchr/testify v1.2.2 // indirect
	gopkg.in/yaml.v2 v2.2.1
)
